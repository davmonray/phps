<div align="center">
<img src="./assets/phps-logo.jpg" width="20%"><br><br>
<b>PHPS: A simple php script to execute differents versions of php in docker containers (only for development !)</b>
</div>
<br><br>

### Installation
```shell
git clone https://gitlab.com/davmonray/phps.git && \
cd phps && sh ./install.sh
```

### Use cases examples
- `phps 81 -v` : to execute php version 8.1 with -v arguments
- `phps 81 php` : run php cli example `phps 81 php -S localhost:8000`
- `phps 81 composer install` : run composer with php version you want
- `phps 82 symfony serve` : run symfony server with php 8.2

### Additional parameters
- `--rebuild` for rebuilding php image, ex: `phps 81 --rebuild`
- `--update` to update phps, ex: `phps --update`

### Optional
- Add php additional configuration in `phps-docker/ini/php.ini` file