#!/bin/bash

if [[ "$1" == "--update" ]]; then
    project_dir=$(which phps)
    if [[ -n "$project_dir" ]]; then
        project_dir=$(readlink "$project_dir")
        project_dir=$(dirname "$project_dir")
        # shellcheck disable=SC2164
        cd "$project_dir"
        git pull
        echo 'Phps à été mis à jour'
    else
      "Le chemin du projet Phps n'a pas été trouvé"
    fi
    exit 1
fi

if ! [[ "$1" =~ ^[0-9]+$ ]]; then
  echo "Erreur : Le premier paramètre doit être un nombre."
  exit 1
fi

phpversion="${1:0:1}.${1:1:1}"
current_user=$(whoami)

if [[ "$2" == "--rebuild" ]]; then
    IMAGE_NAME=phps-"$phpversion"-fpm
    if [[ $(docker images -q "$IMAGE_NAME") ]]; then
        echo "Image $IMAGE_NAME trouvée. Suppression en cours..."
        docker rmi -f "$IMAGE_NAME"
    fi
    echo "Reconstruction de l'image $IMAGE_NAME"
    docker build -t phps-"$phpversion"-fpm --build-arg PHP_VERSION="$phpversion" --build-arg USER="$current_user" /usr/share/phps-docker/.
    exit 1
fi

shift
rest_args="$@"

if docker images | grep -q "phps-$phpversion-fpm"; then
    docker run --rm --interactive --tty \
    --network host \
    --volume /usr/share/phps-docker/ini/php.ini:/usr/local/etc/php/conf.d/custom-php.ini \
    --volume /usr/share/phps-docker/ini/php-fpm.ini:/usr/local/etc/php/conf.d/custom-php-fpm.ini \
    --volume $PWD:/app -v ~/.ssh:/home/"$current_user"/.ssh \
    phps-"$phpversion"-fpm $rest_args
else
  echo "Aucune image Docker trouvée avec le tag $phpversion-fpm. Construction de l'image..."
  docker build -t phps-"$phpversion"-fpm --build-arg PHP_VERSION="$phpversion" --build-arg USER="$current_user" /usr/share/phps-docker/.
  if [ $? -eq 0 ]; then
     docker run --rm --interactive --tty \
        --network host \
        --volume /usr/share/phps-docker/ini/php.ini:/usr/local/etc/php/conf.d/custom-php.ini \
        --volume /usr/share/phps-docker/ini/php-fpm.ini:/usr/local/etc/php/conf.d/custom-php-fpm.ini \
        --volume "$PWD":/app -v ~/.ssh:/home/"$current_user"/.ssh \
        phps-"$phpversion"-fpm $rest_args
  else
    echo "Échec de la construction de l'image Docker."
    exit 1
  fi
fi
